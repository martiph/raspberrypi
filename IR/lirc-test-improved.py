import RPi.GPIO as GPIO
import lirc

try:
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(21,GPIO.OUT)

    connection = lirc.client.RawConnection()
    
    while True:
        line = (connection.readline())
        if "UP" in line:
            GPIO.output(21,GPIO.HIGH)
        elif "DOWN" in line:
            GPIO.output(21, GPIO.LOW)
        print line
except:
    GPIO.cleanup()
