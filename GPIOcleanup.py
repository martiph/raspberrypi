import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(True)
gpios = [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
try:
    for gpio in gpios:
        GPIO.setup(gpio,GPIO.OUT)
        GPIO.output(gpio,GPIO.LOW)
        time.sleep(1)
        GPIO.output(gpio,GPIO.HIGH)
        print("Setup GPIO" + str(gpio))
        time.sleep(1)
finally:
    GPIO.cleanup()
