import RPi.GPIO as GPIO
import time

button_gpio = 14

GPIO.setmode(GPIO.BCM)
GPIO.setup(button_gpio, GPIO.IN)
try:
    while(True):
        print(GPIO.input(button_gpio))
        time.sleep(1)
finally:
    GPIO.cleanup()