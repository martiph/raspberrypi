import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(True)
GPIO.setup(14,GPIO.OUT)
i=0
try:
    while(1<100):
        GPIO.output(14,GPIO.HIGH)
        time.sleep(1)
        GPIO.output(14,GPIO.LOW)
        time.sleep(1)
        i += 1
finally:
    GPIO.cleanup()
