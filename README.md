# RaspberryPi

Some Scripts to use with the raspberry pi and some sensors / additional hardware

## Some links

[Play sound (mp3) using SoX](http://www.python-exemplarisch.ch/index_de.php?inhalt_links=navigation_de.inc.php&inhalt_mitte=raspi/de/sound.inc.php)

[Install gunicorn and launch django application](http://www.oneclicksimplify.com/jessie.html)
