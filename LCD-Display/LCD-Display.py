import time
import RPi.GPIO as GPIO
from RPLCD.gpio import CharLCD

try:
    #lcd = CharLCD(cols=16, rows=2, pin_rs=26, pin_e=19, pins_data=[21, 20, 16, 12, 13, 6, 5, 11], numbering_mode=GPIO.BCM)
    lcd = CharLCD(cols=16, rows=2, pin_rs=26, pin_e=19, pins_data=[13, 6, 5, 11], numbering_mode=GPIO.BCM)
    print ("LCD initialised")

    lcd.write_string('Hello World ääääöööüüü!')
    print ("String written")
    time.sleep(60)
finally:
    lcd.close(clear=True)
    GPIO.cleanup()
