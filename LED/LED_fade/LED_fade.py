#!/usr/bin/env python3
from RPi import GPIO
import time

RedLED = 10
GPIO.setmode(GPIO.BCM)
GPIO.setup(RedLED, GPIO.OUT)
p = GPIO.PWM(RedLED, 50) #f = 50Hz
p.start(0)

print("Start")
dc = 0
count = 0
try:
    while True:
        if(count>100):
            GPIO.OUTPUT(RedLED, False)
            print("OFF")
        else:
            count =+ count
            if(dc < 100):
                dc = dc + 5
            else:
                dc = 0
            p.ChangeDutyCycle(dc)
            print("ON %d " %dc)
        time.sleep(1)
except KeyboardInterrupt:
    GPIO.cleanup() # clean up GPIO on CTRL+C exit
GPIO.cleanup() # clean up GPIO on normal exit
print("Ende")
