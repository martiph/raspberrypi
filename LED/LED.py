import RPi.GPIO as GPIO
import time

led_gpio = 15

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(True)

GPIO.setup(led_gpio,GPIO.OUT)
try:
    print("LED on")
    GPIO.output(led_gpio,GPIO.HIGH)
    time.sleep(100)
    GPIO.output(led_gpio,GPIO.LOW)
    print("LED off")
finally:
    GPIO.cleanup()